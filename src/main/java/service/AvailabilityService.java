package service;

public interface AvailabilityService {
    public Boolean isAvailable();
}
