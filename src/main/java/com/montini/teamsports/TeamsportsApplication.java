package com.montini.teamsports;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeamsportsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeamsportsApplication.class, args);
    }

}
