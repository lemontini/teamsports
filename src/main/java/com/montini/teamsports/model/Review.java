package com.montini.teamsports.model;

import java.sql.Timestamp;

public class Review {
    private int userId;
    private Timestamp timestamp;
    private String description;
}
